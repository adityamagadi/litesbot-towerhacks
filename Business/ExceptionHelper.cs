﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ExceptionHelper
    {
        public static void GaurdArgumentNull(string paramName, object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(paramName);
            }
        }
    }
}
