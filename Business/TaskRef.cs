﻿using Persistence;

namespace Business
{
    public class TaskRef
    {
        private TaskDTO _Config;
        private TaskDAO _conf;
        public TaskRef()
        { _Config = new TaskDTO(); _conf = new TaskDAO(); }
        public TaskDTO GetTask(string taskNo)
        {
            return _conf.RetrieveMonitoringTask(taskNo);
        }
    }
}
