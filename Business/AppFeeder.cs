﻿using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class AppFeeder
    {
        private ConfigurationDTO _Config;
        private ConfigurationDAO _conf;
        public AppFeeder()
        { _Config = new ConfigurationDTO(); _conf = new ConfigurationDAO(); }

        public ConfigurationDTO GetFeederCount(string feederKey, string application)
        {
            return _conf.GetFeeder( feederKey, application);
        }

        public bool UpdateFeederValue(string AppKey, string AppValue, string Application)
        {
            return _conf.UpdateFeederCount(AppKey, AppValue, Application);
        }
       
    }
}
