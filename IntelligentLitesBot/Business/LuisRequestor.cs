﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace IntelligentLitesBot.Business
{
    public class LuisRequestor
    {
        public static async Task<Luis.LitesBot> ParseTextMessage(string message)
        {
            message = Uri.EscapeDataString(message);
            using (var client = new HttpClient())
            {
                string url = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/e2fd0dc9-781d-469c-bba8-eec3cdb0fefa?subscription-key=775742cd240d411c91ae965d6327780c&timezoneOffset=0&verbose=true&q=" + message;
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    var jsonData = JsonConvert.DeserializeObject<Luis.LitesBot>(jsonResponse);
                    return jsonData;
                }
            }
            return null;
        }


    }
}