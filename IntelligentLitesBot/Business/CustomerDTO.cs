﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

namespace IntelligentLitesBot.Business
{
    public class CustomerDTO
    {
        public int CustomerID { get; set; }
        public string CustomerFullName { get; set; }
        public decimal? Allocation { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? Effdt { get; set; }
        public DateTime? Termdt { get; set; }
    }
}