﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntelligentLitesBot.Business
{
    public class CLaimsDTO
    {
        public string CustomerName { get; set; }
        public string ClaimDate { get; set; }
        public decimal? ClaimAmount { get; set; }
    }
}