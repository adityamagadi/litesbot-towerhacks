//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntelligentLitesBot.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public int CustomerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string email { get; set; }
        public string Mobile { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<decimal> Allocation { get; set; }
        public Nullable<System.DateTime> Effdt { get; set; }
        public Nullable<System.DateTime> Termdt { get; set; }
    }
}
