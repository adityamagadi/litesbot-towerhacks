﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Linq;
using System.Text;

using System.Configuration;

namespace IntelligentLitesBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;
            var requestResponse = "";
            StringBuilder ClaimsData = new StringBuilder();
            string user = ConfigurationManager.AppSettings["defaultUser"];
            try
            {
                var luisData = await Business.LuisRequestor.ParseTextMessage(activity.Text);
                //member is active and its his customer id after authentication.
                int customerID = Convert.ToInt16(ConfigurationManager.AppSettings["defaultCustomerId"]);
                if (luisData.intents.Any())
                {
                    switch (luisData.intents[0].intent)
                    {
                        case "Claim details":
                            if (luisData.entities.Any())
                            {
                                ClaimStatus claim = new ClaimStatus();
                                ClaimsData=claim.GetClaimStatusResponse(luisData, customerID);
                            }
                            else
                            {
                                ClaimsData.AppendLine("sorry i do not understand the request, please try again.");
                            }
                            break;
                        case "lites status":
                            if (luisData.entities.Any())
                            {
                                LitesStatus status = new LitesStatus();
                                ClaimsData=status.GetLitesStatusResponse(luisData, user);
                            }
                            else
                            {
                                ClaimsData.Append("sorry i do not understand the request, please try again.");
                            }
                            break;
                        case "Customer":
                            if (luisData.entities.Any())
                            {
                                CustomerStatus customer = new CustomerStatus();
                                ClaimsData= customer.GetClaimStatusResponse(luisData, customerID);
                            }
                            else
                            {
                                ClaimsData.Append("sorry i do not understand the request, please try again.");
                            }
                            break;
                        case "Greetings":
                            var temp = luisData.entities.FirstOrDefault();
                            if (luisData.entities.FirstOrDefault() != null)
                            {
                                ClaimsData.AppendLine(string.Format("You are welcome {0}", user));
                            }
                            else
                            {
                                ClaimsData.AppendLine(string.Format("Hi {0}, How may I assist you ? ", user));
                            }
                            break;
                        default:
                            ClaimsData.AppendLine("sorry i do not understand your request, please try again.");
                            break;
                    }
                    requestResponse = ClaimsData.ToString();
                }
            }
            catch (Exception ex)
            {
                //logging exception for any unexpected error.
                Console.WriteLine(ex.InnerException);
            }
            finally
            {
                if (requestResponse == "")
                    requestResponse = "Apologies, I could not find the requested data.";
            }

            // return our reply to the user
            await context.PostAsync($" {requestResponse}");

            context.Wait(MessageReceivedAsync);
        }
        
        /*  old code
        private string RetrieveFeederCount()
        {
            using (var db = new Models.LitesDataEntities1())
            {
                var query = from x in db.Configurations
                            where x.FieldName == "FeederCount"
                            select x.FieldValue;

                return query.FirstOrDefault().ToString();
            }
        }
        private void UpdateFeederCount(int feederValue)
        {
            using (var db = new Models.LitesDataEntities1())
            {
                var feeder = db.Configurations.Where(x => x.Id == 1).FirstOrDefault();
                feeder.FieldValue = feederValue.ToString();
                db.SaveChanges();
            }
        }
        */
        
    }
}