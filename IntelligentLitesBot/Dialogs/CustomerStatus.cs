﻿using Business;
using IntelligentLitesBot.Business;
using IntelligentLitesBot.Luis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace IntelligentLitesBot.Dialogs
{
    public class CustomerStatus
    {
        public StringBuilder GetClaimStatusResponse(LitesBot luisData, int customerID)
        {
            StringBuilder ClaimsData = new StringBuilder();
            if (luisData.entities[0].type.Equals("CustomerBalance"))
            {
                ClaimsData.AppendLine("Following is your balance details:");
                ClaimsData.Append(Environment.NewLine);
                var cust = RetrieveCustomerBalance(customerID);
                ClaimsData.Append(FormatCustomerResult(cust));
            }
            else if (luisData.entities.FirstOrDefault().type.Equals("CustomerEligibility"))
            {
                var cust = RetrieveCustomerBalance(customerID);
                ClaimsData.AppendLine("Following is your plan details:");
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.Append(FormatCustomerResult(cust));
            }
            else if (luisData.entities.FirstOrDefault().type.Equals("CustomerBenefits"))
            {
                var cust = RetrieveCustomerBalance(customerID);
                ClaimsData.AppendLine("Following are your plan benefits:");
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.Append(FormatCustomerResult(cust));
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.AppendLine("You can take pharmacy at all outlets with 20% discount.");
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.AppendLine("You can make appointments within 2 Weeks.");
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.AppendLine("You are a premium customer.");
            }
            return ClaimsData;
        }
        private StringBuilder FormatCustomerResult(CustomerDTO cust)
        {
            ExceptionHelper.GaurdArgumentNull("cust", cust);
            StringBuilder ClaimsData = new StringBuilder();
            ClaimsData.AppendLine("Name:" + cust.CustomerFullName);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Allocatiton:$" + cust.Allocation);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Balance:$" + cust.Balance);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("From:" + cust.Effdt);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("To:" + cust.Termdt);
            return ClaimsData;
        }

        private Business.CustomerDTO RetrieveCustomerBalance(int CustomerID)
        {
            using (var db = new Models.LitesDataEntities1())
            {
                var query = from x in db.Customers
                            where x.CustomerId == CustomerID
                            select new Business.CustomerDTO { CustomerFullName = x.FName + " " + x.LName, Effdt = x.Effdt, Termdt = x.Termdt, Balance = x.Balance, Allocation = x.Allocation };

                return query.ToList().FirstOrDefault();
            }
        }
    }
}