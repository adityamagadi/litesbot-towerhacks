﻿using Business;
using IntelligentLitesBot.Business;
using IntelligentLitesBot.Luis;
using IntelligentLitesBot.Models;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace IntelligentLitesBot.Dialogs
{
    public class LitesStatus
    {
        public StringBuilder GetLitesStatusResponse(LitesBot luisData, string user)
        {
            StringBuilder ClaimsData = new StringBuilder();
            AppFeeder feeder = new AppFeeder();
            if (luisData.entities[0].type.Equals("feederCount"))
            {
                //feeder.GetFeederCount();
                string feederKey= "MRA_ELIG_FEEDER_ROWCOUNT", application= "LSB01";
                var feedercount = feeder.GetFeederCount(feederKey, application).FieldValue;
                ClaimsData.Append("current feeder count is " + feedercount);
            }
            else if (luisData.entities.FirstOrDefault().type.Equals("feedervalue"))
            {
                var feederValue = luisData.entities[0].entity;
                if (feeder.UpdateFeederValue("MRA_ELIG_FEEDER_ROWCOUNT", feederValue, "LSB01"))
                {
                    ClaimsData.Append("updated feeder count to " + feederValue);
                }
            }
            else if (luisData.entities.FirstOrDefault().type.Equals("TaskNo"))
            {
                TaskRef tref= new TaskRef();
                var taskNo = luisData.entities[0].entity.ToString();
                TaskDTO task = InsertMonitoringTask(taskNo, user, out bool exist);
                if (exist)
                {
                    ClaimsData.AppendLine("Task already exists for Today, Following are the details; ");
                    ClaimsData.Append(Environment.NewLine);
                    ClaimsData.Append(FormatTaskResults(task));
                }
                else if (!exist && task == null)
                {
                    ClaimsData.AppendLine(string.Format("There is no Task {0} for reference", taskNo));
                }
                else
                {
                    ClaimsData.AppendLine("Created task, Following are the details; ");
                    ClaimsData.Append(Environment.NewLine);
                    ClaimsData.Append(FormatTaskResults(task));
                }
                //ClaimsData.Append("Created monitoring task: " + taskNo);
            }
            else if (luisData.entities.FirstOrDefault().type.Equals("TaskStatus"))
            {
                var taskNo = luisData.entities[0].entity.ToString();
                TaskRef tref = new TaskRef();
                TaskDTO task = tref.GetTask(taskNo);
                if (task != null)
                {
                    ClaimsData.AppendLine("Following is the Taskstatus; ");
                    ClaimsData.Append(Environment.NewLine);
                    ClaimsData.Append(FormatTaskResults(task));
                }
                else
                {
                    ClaimsData.AppendLine(string.Format("There is no Task {0} executed today.", taskNo));
                }
            }
            return ClaimsData;


        }
        private TaskDTO InsertMonitoringTask(string taskNo, string user, out bool existing)
        {
            using (var db = new Models.LitesDataEntities1())
            {
                existing = false;
                var ExistingTask = RetrieveMonitoringTask(taskNo);
                if (ExistingTask != null)
                {
                    existing = true;
                    return ExistingTask;
                }
                var query = from x in db.TaskReferences.Where(a => a.TaskNo == taskNo)
                            select new TaskDTO { TaskNo = x.TaskNo, TaskName = x.TaskDescription };
                string taskname;
                if (query.Count() == 0)
                {
                    existing = false;
                    return ExistingTask;
                }
                else
                {
                    taskname = query.ToList().FirstOrDefault().TaskName;
                }
                var task = db.Set<MonitoringTask>();
                task.Add(new MonitoringTask { TaskName = taskname, createid = user, TaskNo = taskNo, Createdt = DateTime.Now, updatedt = DateTime.Now, Status = "Started" });
                db.SaveChanges();
                return RetrieveMonitoringTask(taskNo);
            }
        }
        private StringBuilder FormatTaskResults(TaskDTO task)
        {
            ExceptionHelper.GaurdArgumentNull("task", task);
            StringBuilder ClaimsData = new StringBuilder();
            ClaimsData.AppendLine("Name:" + task.TaskName);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Task No:" + task.TaskNo);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Created by:" + task.Createid);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Createdate:" + task.Createdt);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Status:" + task.Status);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Remarks:" + task.Remarks);
            return ClaimsData;
        }
        private TaskDTO RetrieveMonitoringTask(string taskNo)
        {
            using (var db = new Models.LitesDataEntities1())
            {
                DateTime dt = DateTime.Now;
                var query = from x in db.MonitoringTasks.Where(a => a.TaskNo == taskNo)
                            select new TaskDTO { Id = x.Id, Createdt = x.Createdt, Createid = x.createid, Status = x.Status.ToString(), TaskName = x.TaskName, Remarks = x.Remarks, TaskNo = x.TaskNo };

                var existingtask = query.ToList().Where(x => x.Createdt.Value.Day == dt.Day).OrderByDescending(x => x.Createdt).FirstOrDefault();
                return existingtask;
            }
        }
    }
}