﻿using IntelligentLitesBot.Business;
using IntelligentLitesBot.Luis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace IntelligentLitesBot.Dialogs
{
    public class ClaimStatus
    {
        public StringBuilder GetClaimStatusResponse(LitesBot luisData, int userId)
        {
            StringBuilder ClaimsData = new StringBuilder();
            if (luisData.entities[0].type.ToUpper() == "LATEST")
            {
                var Claims = RetrieveClaimsData(userId);
                if (Claims != null)
                {
                    ClaimsData.AppendLine("Following is your latest claim; ");
                    ClaimsData.Append(Environment.NewLine);
                    ClaimsData.Append(FormatClaimResult(Claims));
                    //requestResponse = ClaimsData.ToString();
                }
                else
                {
                    ClaimsData.AppendLine("You haven't claimed yet.");
                }

            }
            else if (luisData.entities[0].type.ToUpper() == "MONTH")
            {
                string month = luisData.entities[0].entity.ToString();
                var Claims = RetrieveClaimsDataPerMonth(month, userId);
                if (Claims != null)
                {
                    ClaimsData.AppendLine(string.Format("Following are your claim for the month of {0}; ", month));
                    ClaimsData.Append(Environment.NewLine);
                    ClaimsData.Append(FormatClaimsResult(Claims));
                    
                }
                else
                {
                    ClaimsData.AppendLine("You have no claims for the month of  " + luisData.entities[0].entity.ToString());
                }
            }
            return ClaimsData;
        }

        private CLaimsDTO RetrieveClaimsData(int CustomerID)
        {
            using (var db = new Models.LitesDataEntities1())
            {
                var query = from x in db.LClaims
                            where x.CustomerID == CustomerID
                            select new CLaimsDTO { CustomerName = x.CustomerName, ClaimAmount = x.ClaimAmount, ClaimDate = x.CLaimDate.ToString() };

                return query.ToList().OrderByDescending(x => x.ClaimDate).FirstOrDefault();
            }
        }
        private StringBuilder FormatClaimResult(CLaimsDTO cLaimsDTO)
        {
            StringBuilder ClaimsData = new StringBuilder();
            ClaimsData.AppendLine("Customer Name:" + cLaimsDTO.CustomerName);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Claim Date:" + cLaimsDTO.ClaimDate);
            ClaimsData.Append(Environment.NewLine);
            ClaimsData.AppendLine("Amount:$" + cLaimsDTO.ClaimAmount);
            return ClaimsData;
        }
        private List<Business.CLaimsDTO> RetrieveClaimsDataPerMonth(string month, int CustomerID)
        {
            using (var db = new Models.LitesDataEntities1())
            {
                var query = from x in db.LClaims
                            where x.CustomerID == CustomerID
                            select new Business.CLaimsDTO { CustomerName = x.CustomerName, ClaimAmount = x.ClaimAmount, ClaimDate = x.CLaimDate.ToString() };

                return query.ToList().Where(x => Convert.ToDateTime(x.ClaimDate).ToString("MMMM").ToUpper() == month.ToUpper()).OrderByDescending(x => x.ClaimDate).ToList();
            }
        }
        private StringBuilder FormatClaimsResult(List<CLaimsDTO> claims)
        {
            StringBuilder ClaimsData = new StringBuilder();
            foreach (var claim in claims)
            {

                ClaimsData.AppendLine("Customer Name:" + claim.CustomerName);
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.AppendLine("Claim Date:" + claim.ClaimDate);
                ClaimsData.Append(Environment.NewLine);
                ClaimsData.AppendLine("Amount:$" + claim.ClaimAmount);
                ClaimsData.AppendLine(Environment.NewLine);
            }

            return ClaimsData;
        }
    }
}