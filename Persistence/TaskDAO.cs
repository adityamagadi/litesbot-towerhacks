﻿using DataAccessLayer;
using System;
using System.Data;


namespace Persistence
{
    public class TaskDAO : BaseObject
    {
        private DBHelper _dbHelper = null;
        private int _id = Constants.NullInt;
        private string _taskno = Constants.NullString;
        private DateTime _createdt = Constants.NullDateTime;
        private string _taskDescription = Constants.NullString;
        private DateTime _updatedt = Constants.NullDateTime;
        private string _createId = Constants.NullString;
        private string _updateId = Constants.NullString;
        
        public TaskDAO()
        {
            _dbHelper = new DBHelper();
        }

        public override bool MapData(DataRow row)
        {
            _id = GetInt(row, "Id");
            _taskno = GetString(row, "TaskNo");
            _taskDescription = GetString(row, "TaskDescription");
            _createdt = GetDateTime(row, "createdt");
            _updatedt = GetDateTime(row, "updatedt");
            _createId = GetString(row, "createid");
            _updateId = GetString(row, "updateid");
            return base.MapData(row);
        }
        public override bool MapData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                return MapData(dt.Rows[0]);
            }
            else
            {
                return false;
            }
        }
        public TaskDTO RetrieveMonitoringTask(string taskNo)
        {
            string query = "Logging..[GetTaskReference]";
            DBParameterCollection param = new DBParameterCollection();
            param.Add(new DBParameter("@taskNo", taskNo, ParameterDirection.Input));
            //param.Add(new DBParameter("@ApplicationName", application, ParameterDirection.Input));
            try
            {
                DataTable taskref = _dbHelper.ExecuteDataTable(query, "TaskReference", param, CommandType.StoredProcedure); //> 0 ? true : false;
                //var val = _dbHelper.GetParameterValue("@Value",CommandType.StoredProcedure);
                MapData(taskref);
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        //TODO
        public TaskDTO InsertMonitoringTask(string taskNo)
        {
            string query = "Configuration..GetAppSettingsValue";
            DBParameterCollection param = new DBParameterCollection();
            param.Add(new DBParameter("@taskNo", taskNo, ParameterDirection.Input));
            //param.Add(new DBParameter("@ApplicationName", application, ParameterDirection.Input));
            try
            {
                DataTable taskref = _dbHelper.ExecuteDataTable(query, "TaskReference", param, CommandType.StoredProcedure); //> 0 ? true : false;
                //var val = _dbHelper.GetParameterValue("@Value",CommandType.StoredProcedure);
                MapData(taskref);
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
