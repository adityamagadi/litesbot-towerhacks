﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Persistence
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string TaskName { get; set; }
        public string TaskNo { get; set; }
        public DateTime? Createdt { get; set; }
        public string Createid { get; set; }
        public DateTime? Updatedt { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
    }
}