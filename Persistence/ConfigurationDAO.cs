﻿using DataAccessLayer;
using System;
using System.Data;

namespace Persistence
{
    public class ConfigurationDAO : BaseObject
    {
        private DBHelper _dbHelper = null;
        private int _id = Constants.NullInt;
        private string _fieldName = Constants.NullString;
        private string _fieldValue = Constants.NullString;
        private DateTime _dob = Constants.NullDateTime;

        public ConfigurationDAO()
        {
            _dbHelper = new DBHelper();
        }

        public ConfigurationDTO GetFeeder(string feederKey, string application)
        {
            ConfigurationDTO conf = new ConfigurationDTO();
            string Keyvalue="";
            string query = "Configuration..GetAppSettingsValue";
            DBParameterCollection param = new DBParameterCollection();
            param.Add(new DBParameter("@KeyName", feederKey, ParameterDirection.Input));
            param.Add(new DBParameter("@ApplicationName", application,ParameterDirection.Input));
            param.Add(new DBParameter("@Value", Keyvalue, DbType.String ,ParameterDirection.Output));
            //DataTable dt=_dbHelper.ExecuteDataTable(query);
            try
            {
                Keyvalue = _dbHelper.ExecuteNonQueryOutputParam(query, param, CommandType.StoredProcedure,"@Value").ToString(); //> 0 ? true : false;
                //var val = _dbHelper.GetParameterValue("@Value",CommandType.StoredProcedure);
                conf.FieldValue = Keyvalue;
                conf.ApplicationName = application;
                conf.FieldName = feederKey;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
            //conf.FieldValue = GetString(dt.Rows[0], "Value");
            //conf.Id= GetInt(dt.Rows[0],"ConfigId");
            return conf;
        }

        public bool UpdateFeederCount(string feederKey, string feederValue,string application)
        {
            string query = "Configuration..UpdateAppSettingsKeyValue";
            DBParameterCollection param= new DBParameterCollection();
            param.Add(new DBParameter("@KeyName", feederKey));
            param.Add(new DBParameter("@KeyValue", feederValue));
            param.Add(new DBParameter("@ApplicationName", application));
            param.Add(new DBParameter("@UpdateID", "Bot"));
            IDbTransaction transaction = _dbHelper.BeginTransaction();
            bool ret;
            try
            {
                ret= _dbHelper.ExecuteNonQuery(query, param, transaction,CommandType.StoredProcedure)>0?true:false;
                _dbHelper.CommitTransaction(transaction);
            }
            catch (Exception err)
            {
                //log error.
                _dbHelper.RollbackTransaction(transaction);
                ret = false;
            }
            return ret;
        }
        public override bool MapData(DataRow row)
        {
            _id = GetInt(row, "Id");
            _fieldName = GetString(row, "FieldName");
            _fieldValue = GetString(row, "FieldValue");
            return base.MapData(row);
        }
        public override bool MapData(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                return MapData(dt.Rows[0]);
            }
            else
            {
                return false;
            }
        }
        
    }
}
